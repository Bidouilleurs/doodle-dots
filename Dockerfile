FROM node:latest
# Working directory for application
WORKDIR /usr/src/app
EXPOSE 10010
# Creates a mount point
VOLUME [ "/usr/src/app" ]
RUN npm i
