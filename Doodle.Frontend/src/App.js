import React, { Component } from 'react';

import {subscribeToTimer} from "./api";

import logo from './logo.svg';
import './App.css';

class App extends Component {

  constructor(props){
    super(props);

    this.state = {
      timestamp: 'no timestamp yet'
    }

    subscribeToTimer(10, (err, timestamp) => {
      this.setState({timestamp})
    });
  }


  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          Time value: {this.state.timestamp}
        </p>
      </div>
    );
  }
}

export default App;
