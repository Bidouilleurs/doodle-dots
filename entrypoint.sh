#!/bin/bash
while !</dev/tcp/db/5432; do sleep 1; done;

# db-migrate db:create event_zoo --config config/db.json -e pg-docker
# db-migrate db:create test --config config/db.json -e pg-docker
npm install;
npm start;
